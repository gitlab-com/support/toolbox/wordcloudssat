from wordcloud import WordCloud
from wordcloud import STOPWORDS
from wordcloud import ImageColorGenerator

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

feedback_text_file = 'feedback.txt'

with open(feedback_text_file) as f:
    cloud_text = f.read()

font = './BubbleShine.otf'
mask = np.array(Image.open('images1.jpg'))
mask_colors = ImageColorGenerator(mask)

wc = WordCloud(stopwords=STOPWORDS, font_path=font,
    mask=mask, background_color="white",
    max_words=2000, max_font_size=256,
    random_state=42, width=mask.shape[1],
    height=mask.shape[0],color_func=mask_colors)

wc.generate(cloud_text)
plt.imshow(wc, interpolation="bilinear")
plt.axis('off')
#plt.savefig('wc.jpg') 
plt.show()
